# http://crystal-lang.org
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
# Based on ruby.kak

# Detection
# ‾‾‾‾‾‾‾‾‾

hook global BufCreate .*\.cr %{
    set-option buffer filetype crystal
}

# Highlighters
# ‾‾‾‾‾‾‾‾‾‾‾‾

add-highlighter shared/crystal regions
add-highlighter shared/crystal/code default-region group
add-highlighter shared/crystal/double_string region '"' (?<!\\)(\\\\)*"        regions
add-highlighter shared/crystal/char          region "'" (?<!\\)(\\\\)*'        fill string
add-highlighter shared/crystal/backtick      region '`' (?<!\\)(\\\\)*`        regions
add-highlighter shared/crystal/              region '#' '$'                    fill comment
add-highlighter shared/crystal/              region -recurse  < '%[iqrswxIQRSWX]<'   > fill meta
add-highlighter shared/crystal/heredoc       region '<<[-~]?(\w+)' '^\h*(\w+)$' fill string
add-highlighter shared/crystal/division      region '[\w\)\]](/|(\h+/\h+))' '\w' group # Help Kakoune to better detect /…/ literals

add-highlighter shared/crystal/double_string/ default-region fill string
add-highlighter shared/crystal/double_string/interpolation region -recurse \{ \Q#{ \} fill meta

add-highlighter shared/crystal/backtick/ default-region fill meta
add-highlighter shared/crystal/backtick/interpolation region -recurse \{ \Q#{ \} fill meta

add-highlighter shared/crystal/code/ regex \b([A-Za-z]\w*:(?!:))|([$@][A-Za-z]\w*)|((?<!:):(([A-Za-z]\w*[=?!]?)|(\[\]=?)))|([A-Z]\w*|^|\h)\K::(?=[A-Z]) 0:variable
#--------------
##### Operators
add-highlighter shared/crystal/code/ regex \h((<<|<|<=|=|==|===|!=|=~|\|=|&=|\^=|!~|>>|>|>=|\+|\+=|-|\*|/|~|%|&|\|\h|\|\|\h|\^|\*\*|<=>|:|\?|=>|&&)\h)|(!)) 0:operator
add-highlighter shared/crystal/code/ regex \(|\)|\||\{|\}|:: 0:operator

# Classes, Structs, Modules
add-highlighter shared/crystal/code/ regex \b([A-Z]+[A-Za-z0-9?]*)|(\h+\[\])|(\{\}) 0:type

add-highlighter shared/crystal/code/ regex \b[a-z][a-z0-9_]*: 0:attribute

# Symbols
add-highlighter shared/crystal/code/ regex [\h\(]:[a-z][a-z0-9_]* 0:attribute

# Variables
add-highlighter shared/crystal/code/ regex ((\$|@@|@|\b)[a-z~_][a-z0-9_]*)((\.)|(\[)|(\h*($|if|unless|do|\)|\||\]|,))|(\h+(<<|<|<=|=|==|===|!=|=~|!~|>>|>|>=|=>|\+|\+=|-|\*|/|~|%|&|\|\h|\|\|\h|\^|\*\*|<=>|:|\?)\h+)) 1:variable 2:variable

# Functions
add-highlighter shared/crystal/code/ regex ((?:(?:def\h+)|(\.))(([a-z][a-z0-9_!\?]*)|(<<|<|<=|==|===|!=|=~|!~|>>|>|>=|\+|-|\*|/|~|%|&|\|\h|\^|\*\*|<=>)(?=\())|(\b[a-z][a-z0-9_!\?]*(?=\())|(\b[a-z][a-z0-9_!\?]*\h*\h(?=[a-z0-9]))) 0:function
add-highlighter shared/crystal/code/ regex \[(?!\])|(?<!\[)\] 0:function

# Constants
add-highlighter shared/crystal/code/ regex (?<!::)\b([A-Z][A-Z0-9_]*\b)\b(?!::) 0:value

# Numbers
add-highlighter shared/crystal/code/ regex \b(-?[0-9][0-9_]*([0-9_.]*[0-9_]+)?)(([iu](8|16|32|64))|(f(32|64)))?\b 0:value

evaluate-commands %sh{
    # Grammar
    keywords="abstract|alias|and|begin|break|case|class|def|defined|do|else|elsif|end"
    keywords="${keywords}|ensure|enum|exit|false|for|if|in|loop|module|next|nil|not|of|or|private|protected|redo"
    keywords="${keywords}|rescue|retry|return|self|spawn|struct|super|then|true|undef|uninitialized|unless|until|when|while"
    attributes="getter|setter|property"
    values="false|true|nil"
    meta="require|include|extend"

    # Add the language's grammar to the static completion list
    printf %s\\n "hook global WinSetOption filetype=crystal %{
        set-option window static_words ${keywords} ${attributes} ${values} ${meta}
    }" | tr '|' ' '

    # Highlight keywords
    printf %s "
        add-highlighter shared/crystal/code/ regex \b(${keywords})\b 0:keyword
        add-highlighter shared/crystal/code/ regex \b(${attributes})\b 0:attribute
        add-highlighter shared/crystal/code/ regex \b(${values})\b 0:value
        add-highlighter shared/crystal/code/ regex \b(${meta})\b 0:meta
    "
}

# Commands
# ‾‾‾‾‾‾‾‾

define-command -hidden crystal-filter-around-selections %{
    evaluate-commands -no-hooks -draft -itersel %{
        execute-keys <a-x>
        # remove trailing white spaces
        try %{ execute-keys -draft s \h + $ <ret> d }
    }
}

define-command -hidden crystal-indent-on-char %{
    evaluate-commands -no-hooks -draft -itersel %{
        # align middle and end structures to start
        try %{ execute-keys -draft <a-x> <a-k> ^ \h * (else|elsif) $ <ret> <a-\;> <a-?> ^ \h * (if)                                                       <ret> s \A | \z <ret> \' <a-&> }
        try %{ execute-keys -draft <a-x> <a-k> ^ \h * (when)       $ <ret> <a-\;> <a-?> ^ \h * (case)                                                     <ret> s \A | \z <ret> \' <a-&> }
        try %{ execute-keys -draft <a-x> <a-k> ^ \h * (rescue)     $ <ret> <a-\;> <a-?> ^ \h * (begin)                                                    <ret> s \A | \z <ret> \' <a-&> }
        try %{ execute-keys -draft <a-x> <a-k> ^ \h * (end)        $ <ret> <a-\;> <a-?> ^ \h * (begin|case|((abstract\h+)?class)|(?<!abstract\h)def|do\h*$|for|if|module|unless|until|while) <ret> s \A | \z <ret> \' <a-&> }
        try %{ execute-keys -draft <a-x> <a-k> ^ \h * (end|else|elsif|when|rescue|ensure) $ <ret> < }
    }
}

define-command -hidden crystal-indent-on-new-line %{
    evaluate-commands -no-hooks -draft -itersel %{
        # preserve previous line indent
        try %{ execute-keys -draft K <a-&> }
        # filter previous line
        try %{ execute-keys -draft k : crystal-filter-around-selections <ret> }
        # indent after start structure
        try %{ execute-keys -draft k <a-x> <a-k> ^ \h * (begin|case|((abstract\h+)?class)|(?<!abstract\h)(private\h+|protected\h+)?def|else|elsif|enum|ensure|for|if|module|rescue|(abstract\h+)?struct|unless|until|when|while)\b|(do\h*$|(.*\h+do(\h+\|[^\n]*\|)?\h*$)) <ret> j <a-gt> }
        # property, setter, getter
        try %{ execute-keys -draft k <a-x> <a-k> ^ \h * (property|setter|getter) \b .* , \h* $ <ret> j <a-gt> }
        try %{
          #previous line is empty, next is not
          execute-keys -draft k <a-x> 2X <a-k> \A\n\n[^\n]+\n\z <ret>
          #copy indent of next line
          execute-keys -draft j <a-x> s ^\h+ <ret> y k P
        }
    }
}

# The first argument contains the start structure filtering regex,
# the second contains the end structure filtering regex.
define-command -params 2 crystal-insert-end-sctructure %{ try %{
  # Save arguments to 'a' and 'b' registers. Content of registers can be inserted into command line.
  set-register a %arg{1}
  set-register b %arg{2}

  # Save indent of previous line to 'i' register
  try %{ execute-keys -draft k <a-x> s ^\h+ <ret> \"i y } catch %{ reg i '' }

  # Check if previous line contains a start structure.
  execute-keys -draft k <a-x> <a-k> ^ <c-r>i <c-r>a <ret>

  # Check for closing structure
  execute-keys -draft k gh Ge <a-K> \A <c-r>i <c-r>a \n ( (^<c-r>i\h+[^\n]*)? \n)* <c-r>i <c-r>b <ret>

  # Insert 'end' keyword
  execute-keys -draft o <c-r>i end <esc>
}}

define-command -hidden crystal-insert-on-new-line %{
    evaluate-commands -no-hooks -draft -itersel %{
        # copy _#_ comment prefix and following white spaces

        try %{ execute-keys -draft k <a-x> s '^\h*#\h*' <ret> y gh j gi i <c-r> '"' }

        # Add missing 'end' structure
        crystal-insert-end-sctructure '[^\n]*\h+do(\h+\|[^\n]+\|)?\h*$'                                  '(rescue|ensure|end)'
        crystal-insert-end-sctructure '(enum|module|(abstract\h+)?(struct|class)|until|while)\b[^\n]+$'  'end'
        crystal-insert-end-sctructure 'case\b[^\n]+$'                                                    '(when|end)'
        crystal-insert-end-sctructure '((private\h+|protected\h+)?def|begin)\b[^\n]*$'                   '(rescue|ensure|end)'
        crystal-insert-end-sctructure 'rescue'                                                           '(ensure|end)'
        crystal-insert-end-sctructure 'ensure'                                                           'end'
        crystal-insert-end-sctructure '(unless|if)\b[^\n]+$'                                             '(else|elsif|end)'
    }
}

# Initialization
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾

hook global WinSetOption filetype=crystal %{
    hook window InsertChar .* -group crystal-indent crystal-indent-on-char
    hook window InsertChar \n -group crystal-indent crystal-indent-on-new-line
    hook window InsertChar \n -group crystal-insert crystal-insert-on-new-line
}

hook -group crystal-highlight global WinSetOption filetype=crystal %{ add-highlighter window/crystal ref crystal }

hook -group crystal-highlight global WinSetOption filetype=(?!crystal).* %{ remove-highlighter window/crystal }

hook global WinSetOption filetype=(?!crystal).* %{
    remove-hooks window crystal-indent
    remove-hooks window crystal-insert
}
