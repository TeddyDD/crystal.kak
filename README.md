# `crystal.kak`
[Kakoune](https://kakoune.org/) language file for editing [Crystal](https://crystal-lang.org/) code.

Variables and functions are not always distinguishable by syntax, it mostly depends on context, so Kakoune sometimes highlights them with the wrong color.
